<?php

namespace app\controllers;

use Yii;

use yii\web\Request;

use app\models\Data;

use app\models\Students;

use app\models\DataRequired;

use yii\web\Session;

// print_r(Yii::$app->request->post());
// die();

class FormController extends \yii\web\Controller
{
    public function beforeAction(){

        Yii::$app->controller->enableCsrfValidation = false;

        return true;
    }

    public $salt = 'gs37i@UH8*93h%7sdj(&*#H4jdwoK3Yhnw*(2000Kdjwq';


    public function hisEditor(){

        $sessionValue = Yii::$app->session->get('edit');

        $getValue = Yii::$app->request->get('edit');

        return (!empty($sessionValue) && !empty($getValue) && ($getValue == $sessionValue));
    }

    public function actionIndex(){

        $errors = false;

        $dataModel = new Data;

        $dataRequiredModel = new DataRequired;

        $studensModel = new Students;

        $dataEdit = false;

        if (Yii::$app->request->isPost) {

            if ( $this->hisEditor() )
                $dataModel = Data::find()->where(['hash' => Yii::$app->request->get('edit')])->one();

            $postData = Yii::$app->request->post();

            $studentPostData = $studensModel->prepeareData($postData['s']);

            $dataModel->attributes = $postData;

            foreach ($studentPostData as $key => $value) {

                $studensModel->attributes = $value;

                if ( !$studensModel->validate() ) {

                    $errors['studentError'][$key] = $studensModel->errors;

                    unset($studentPostData[$key]);

                }
            }
            
            if ( $dataModel->validate() ) {
                
                $dataRequiredModel->attributes = $postData;

                $dataRequiredModel->validate() ? $dataModel->status = 'fully' : $dataModel->status = 'save';

                if (isset($postData['methodSub']) && $postData['methodSub'] == 'SAVE'){

                    $dataModel->status = 'save';

                }

                $data_id = $dataModel->saveData();

                $hash = md5($this->salt.base64_encode($data_id));

                $dataModel->hash = $hash;

                $dataModel->save();

                $studensModel->saveData($studentPostData, $data_id);


                if ( !Yii::$app->request->get('edit') && $dataModel->status ==  'save' )
                    return $this->redirect('/form/save?id='.$hash);

                if ($dataModel->status ==  'fully')
                    return $this->redirect('/form/success?id='.$hash);

            }else{

                $errors['dataError'] = $dataModel->errors;

            }
            
        }

        $studentsFees = false;

        if ( Yii::$app->request->get('edit') ) {

            if (Yii::$app->session->get('edit') == Yii::$app->request->get('edit')) {

               $data = Data::find(['*'])->where(['hash' => Yii::$app->request->get('edit')])->one();

               $studentsFees = Students::find()->where(['data_id' => $data->id])->all();

                if (isset($data->id) && $data->id) 
                    $dataEdit = $data;
                else
                    return $this->redirect('/form');

            }else
                return $this->redirect('/form');

        }

        return $this->render('index', [
                'errors' => $errors,
                'dataEdit' => $dataEdit,
                'studentsFees' => $studentsFees
            ]);

    }


    public function actionSave(){
        if ( !empty(Yii::$app->request->get('id')) && $data = Data::find()->where(['hash' => Yii::$app->request->get('id')])->one() ) 
            return $this->render('save', ['data' => $data]);
        else
            return $this->redirect('/');
    }

    public function actionSuccess(){

        if ( Yii::$app->request->get('id') ) {


           $data = Data::find(['*'])->where(['hash' => Yii::$app->request->get('id')])->one();

            if (isset($data->id) && $data->id) 
                $dataEdit = $data;
            else
                return $this->redirect('/form');


        }else
            return $this->redirect('/form');

        return $this->render('success', ['data' => $dataEdit]);
    }

    public function actionEdit(){

        $error = false;


        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();

            if (isset($post['password']) && isset($post['id'])) {

               $data = Data::find()->where(['id' => $post['id']])->andWhere(['password' => $post['password']])->one();

               if (isset($data->id) && !empty($data->id)) {

                   Yii::$app->session->set('edit', $data->hash);

                   $this->redirect('/form?edit='.$data->hash);

               }
               else
                    $error = true;

            }else
                $error = true;
        }

        

        return $this->render('edit', ['error' => $error]);

    }

}
