<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Data */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'director_first_name',
            'director_middle_initial',
            'director_last_name',
            'home_phone',
            'address:ntext',
            'city:ntext',
            'state',
            'zip',
            'email:email',
            'password:ntext',
            'directors_main_instrument:ntext',
            'new_member',
            'school_name',
            'school_phone',
            'principals_name',
            'past_president',
            'fax',
            'school_address',
            'school_city',
            'school_state',
            'school_zip',
            'i_ll_help_with_auditions',
            'i_m_playing_in_reading_band',
            'do_not_publish_my_home_contact',
            'do_not_share_my_email:email',
            'membership_type',
            'annual_dues',
            'pre_registration_member',
            'pre_registration_non_member',
            'banquet',
            'banquet_attendings_number',
            'payment_options',
            'payment_status',
            'status',
        ],
    ]) ?>

</div>
