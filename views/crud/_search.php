<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CrudData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'director_first_name') ?>

    <?= $form->field($model, 'director_middle_initial') ?>

    <?= $form->field($model, 'director_last_name') ?>

    <?= $form->field($model, 'home_phone') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'zip') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'directors_main_instrument') ?>

    <?php // echo $form->field($model, 'new_member') ?>

    <?php // echo $form->field($model, 'school_name') ?>

    <?php // echo $form->field($model, 'school_phone') ?>

    <?php // echo $form->field($model, 'principals_name') ?>

    <?php // echo $form->field($model, 'past_president') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'school_address') ?>

    <?php // echo $form->field($model, 'school_city') ?>

    <?php // echo $form->field($model, 'school_state') ?>

    <?php // echo $form->field($model, 'school_zip') ?>

    <?php // echo $form->field($model, 'i_ll_help_with_auditions') ?>

    <?php // echo $form->field($model, 'i_m_playing_in_reading_band') ?>

    <?php // echo $form->field($model, 'do_not_publish_my_home_contact') ?>

    <?php // echo $form->field($model, 'do_not_share_my_email') ?>

    <?php // echo $form->field($model, 'membership_type') ?>

    <?php // echo $form->field($model, 'annual_dues') ?>

    <?php // echo $form->field($model, 'pre_registration_member') ?>

    <?php // echo $form->field($model, 'pre_registration_non_member') ?>

    <?php // echo $form->field($model, 'banquet') ?>

    <?php // echo $form->field($model, 'banquet_attendings_number') ?>

    <?php // echo $form->field($model, 'payment_options') ?>

    <?php // echo $form->field($model, 'payment_status') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
