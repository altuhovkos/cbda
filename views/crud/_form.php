<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Data */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'director_first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'director_middle_initial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'director_last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'home_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'city')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'directors_main_instrument')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'new_member')->textInput() ?>

    <?= $form->field($model, 'school_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'school_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'principals_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'past_president')->textInput() ?>

    <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'school_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'school_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'school_state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'school_zip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'i_ll_help_with_auditions')->textInput() ?>

    <?= $form->field($model, 'i_m_playing_in_reading_band')->textInput() ?>

    <?= $form->field($model, 'do_not_publish_my_home_contact')->textInput() ?>

    <?= $form->field($model, 'do_not_share_my_email')->textInput() ?>

    <?= $form->field($model, 'membership_type')->textInput() ?>

    <?= $form->field($model, 'annual_dues')->textInput() ?>

    <?= $form->field($model, 'pre_registration_member')->textInput() ?>

    <?= $form->field($model, 'pre_registration_non_member')->textInput() ?>

    <?= $form->field($model, 'banquet')->textInput() ?>

    <?= $form->field($model, 'banquet_attendings_number')->textInput() ?>

    <?= $form->field($model, 'payment_options')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_status')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
