<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CrudData */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Datas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'director_first_name',
            'director_middle_initial',
            'director_last_name',
            'home_phone',
            // 'address:ntext',
            // 'city:ntext',
            // 'state',
            // 'zip',
            // 'email:email',
            // 'password:ntext',
            // 'directors_main_instrument:ntext',
            // 'new_member',
            // 'school_name',
            // 'school_phone',
            // 'principals_name',
            // 'past_president',
            // 'fax',
            // 'school_address',
            // 'school_city',
            // 'school_state',
            // 'school_zip',
            // 'i_ll_help_with_auditions',
            // 'i_m_playing_in_reading_band',
            // 'do_not_publish_my_home_contact',
            // 'do_not_share_my_email:email',
            // 'membership_type',
            // 'annual_dues',
            // 'pre_registration_member',
            // 'pre_registration_non_member',
            // 'banquet',
            // 'banquet_attendings_number',
            // 'payment_options',
            // 'payment_status',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
