<?php if (isset($studentsFees) && !empty($studentsFees) && is_array($studentsFees)): ?>
    <?php foreach ($studentsFees as $key => $student): ?>
    	<input type="hidden" name="s[ifRemoveId][]" value='<?= isset($student->id) ? $student->id : false; ?>'>
		<div class="student_fee">
    	<input type="hidden" name="s[id][]" value='<?= isset($student->id) ? $student->id : false; ?>'>
		    <table>
		        <tr>
		            <td colspan="4" class="studentHead">Student <span class="student_number"><?= $key + 1 ?></span> Information <span class="delete_student" style="display:<?php if($key != 0): ?> block <?php else: ?> none <?php endif; ?>;"><a href="#" >DELETE STUDENT</a></span></td>
		        </tr>

		        <tr>
		            <th><b>Band:</b></th>
		            <td>
		                <select name="s[band][]">
		                    <option <?php if(!$student->band): ?> selected <?php endif; ?> value="">Select One</option>
		                    <option <?php if($student->band == 2): ?> selected <?php endif; ?> value="2">CBDA High School All-State Honor Band</option>
		                    <option <?php if($student->band == 4): ?> selected <?php endif; ?> value="4">CODA High School All-State Orchestra</option>
		                    <option <?php if($student->band == 5): ?> selected <?php endif; ?> value="5">Either the All-State Band OR the All-State Orchestra</option>
		                    <option <?php if($student->band == 1): ?> selected <?php endif; ?> value="1">CBDA Junior High School All-State Honor Bands (Symphonic, Concert)</option>
		                    <option <?php if($student->band == 3): ?> selected <?php endif; ?> value="3">CBDA/CAJ High School All-State Jazz Band</option>
		                    <option <?php if($student->band == 6): ?> selected <?php endif; ?> value="6">CBDA/CAJ Junior High All State Jazz Band</option>
		                </select>
		            </td>
		        </tr>
		        <tr>
		            <th>
		                <b>Instrument:</b>
		            </th>
		            <td>
		                <select name="s[instrument][]">
		                    <option <?php if(!$student->instrument): ?> selected <?php endif; ?> value="">Select One</option>
		                    <option <?php if($student->instrument == 49): ?> selected <?php endif; ?> value="49">Alto Saxophone</option>
		                    <option <?php if($student->instrument == 50): ?> selected <?php endif; ?> value="50">Tenor Saxophone</option>
		                    <option <?php if($student->instrument == 51): ?> selected <?php endif; ?> value="51">Baritone Saxophone</option>
		                    <option <?php if($student->instrument == 52): ?> selected <?php endif; ?> value="52">Trumpet</option>
		                    <option <?php if($student->instrument == 53): ?> selected <?php endif; ?> value="53">Trombone</option>
		                    <option <?php if($student->instrument == 54): ?> selected <?php endif; ?> value="54">Bass Trombone</option>
		                    <option <?php if($student->instrument == 55): ?> selected <?php endif; ?> value="55">Piano</option>
		                    <option <?php if($student->instrument == 56): ?> selected <?php endif; ?> value="56">Guitar</option>
		                    <option <?php if($student->instrument == 57): ?> selected <?php endif; ?> value="57">Bass</option>
		                    <option <?php if($student->instrument == 58): ?> selected <?php endif; ?> value="58">Drums</option>
		                    <option <?php if($student->instrument == 59): ?> selected <?php endif; ?> value="59">Vibes</option>
		                </select>
		            </td>
		        </tr>
		        <tr>
		            <th><b>Student First Name:</b></th>
		            <td>
		                <span ng-show="(form['s[first_name][]'].$dirty && form['s[first_name][]'].$error.required) || (form.$submitted && form['s[first_name][]'].$error.required)" class='field_error'>Field is requried</span>
		                <input ng-if="form.membershipType == 'membership_with_cd'" name="s[first_name][]" type="text" ng-model="form.first_name[<?= $key ?>]" ng-init="form.first_name[<?= $key ?>] = '<?= isset($student->first_name) ? $student->first_name : false; ?>'" required size='26' />
		            </td>
		        </tr>
		        <tr>
		            <th>Student Middle Initial:</th>
		            <td>
		                <input name="s[middle_initial][]" value="<?= isset($student->middle_initial) ? $student->middle_initial : false; ?>" size="26" />
		            </td>
		        </tr>
		        <tr>
		            <th><b>Student Last Name:</b></th>
		            <td>
		                <span ng-show="(form['s[last_name][]'].$dirty && form['s[last_name][]'].$error.required) || (form.$submitted && form['s[last_name][]'].$error.required)" class='field_error'>Field is requried</span>
		                <input ng-if="form.membershipType == 'membership_with_cd'" name="s[last_name][]" ng-init="form.lastName[<?= $key ?>] = '<?= isset($student->last_name) ? $student->last_name : false; ?>'" type="text" ng-model="form.lastName[<?= $key ?>]" required size='26' />
		            </td>
		        </tr>
		        <tr>
		            <th>
		                <b>Home Phone:</b>
		            </th>
		            <td>
		                <span ng-show="(form['s[home_phone][]'].$dirty && form['s[home_phone][]'].$error.required) || (form.$submitted && form['s[home_phone][]'].$error.required)" class='field_error'>Field is requried</span>
		                <input ui-mask='(999)-999-999' ng-if="form.membershipType == 'membership_with_cd'" ng-init="form.homePhone[<?= $key ?>] = '<?= isset($student->home_phone) ? $student->home_phone : false; ?>'" name="s[home_phone][]" type="text" ng-model="form.homePhone[<?= $key ?>]" required size='26' />
		            </td>
		        </tr>
		        <tr>
		            <th>
		                <b>Address:</b>
		            </th>
		            <td>
		                <span ng-show="(form['s[address][]'].$dirty && form['s[address][]'].$error.required) || (form.$submitted && form['s[address][]'].$error.required)" class='field_error'>Field is requried</span>
		                <input ng-if="form.membershipType == 'membership_with_cd'" ng-init="form.addressStudent[<?= $key ?>] = '<?= isset($student->address) ? $student->address : false; ?>'" name="s[address][]" type="text" ng-model="form.addressStudent[<?= $key ?>]" required size='26' />
		            </td>
		        </tr>
		        <tr>
		            <th>
		                <b>City:</b>
		            </th>
		            <td>
		                <span ng-show="(form['s[city][]'].$dirty && form['s[city][]'].$error.required) || (form.$submitted && form['s[city][]'].$error.required)" class='field_error'>Field is requried</span>
		                <input ng-if="form.membershipType == 'membership_with_cd'" ng-init="form.cityStudent[<?= $key ?>] = '<?= isset($student->city) ? $student->city : false; ?>'" name="s[city][]" type="text" ng-model="form.cityStudent[<?= $key ?>]" required size='26' />
		            </td>
		        </tr>
		        <tr>
		            <th><b>State:</b></th>
		            <td>
		                <input type="hidden" name="state" value="CA">CA</td>
		        </tr>
		        <tr>
		            <th><b>Zip:</b></th>
		            <td>
		                <span ng-show="(form['s[zip][]'].$dirty && form['s[zip][]'].$error.required) || (form.$submitted && form['s[zip][]'].$error.required)" class='field_error'>Field is requried</span>
		                <input ng-if="form.membershipType == 'membership_with_cd'" ng-init="form.zipStudent[<?= $key ?>] = '<?= isset($student->zip) ? $student->zip : false; ?>'" name="s[zip][]" type="text" ng-model="form.zipStudent[<?= $key ?>]" required size='26' />
		            </td>
		        </tr>
		        <tr>
		            <th>Parent Email:</th>
		            <td>
		                <input name="s[parent_email][]" value="<?= isset($student->parent_email) ? $student->parent_email : false; ?>" size="26" />
		            </td>
		        </tr>
		        <tr>
		            <th>Student Email:</th>
		            <td>
		                <input name="s[student_email][]" value="<?= isset($student->student_email) ? $student->student_email : false; ?>" size="26" />
		            </td>
		        </tr>
		        <tr>
		            <th><b>Gender:</b></th>
		            <td>
		                <select name="s[gender][]" _value_="">
		                    <option value="">Select One</option>
		                    <option <?php if($student->gender == 'Male'): ?> selected <?php endif; ?> value="Male">Male</option>
		                    <option <?php if($student->gender == 'Female'): ?> selected <?php endif; ?> value="Female">Female</option>
		                </select>
		            </td>
		        </tr>
		        <tr>
		            <th><b>Current Grade:</b></th>
		            <td>
		                <select name="s[grade_year][]">
		                    <option value="">Select One</option>
		                    <option <?php if($student->grade_year == '7'): ?> selected <?php endif; ?> value="7">7</option>
		                    <option <?php if($student->grade_year == '8'): ?> selected <?php endif; ?> value="8">8</option>
		                    <option <?php if($student->grade_year == '9'): ?> selected <?php endif; ?> value="9">9</option>
		                    <option <?php if($student->grade_year == '10'): ?> selected <?php endif; ?> value="10">10</option>
		                    <option <?php if($student->grade_year == '11'): ?> selected <?php endif; ?> value="11">11</option>
		                    <option <?php if($student->grade_year == '12'): ?> selected <?php endif; ?> value="12">12</option>
		                </select>
		            </td>
		        </tr>
		        <tr>
		            <th><b>Year You Graduate High School:</b></th>
		            <td>
		                <select name="s[high_school_year][]">
		                    <option value="">Select One</option>
		                    <option <?php if($student->high_school_year == '2016'): ?> selected <?php endif; ?> value="2016">2016</option>
		                    <option <?php if($student->high_school_year == '2017'): ?> selected <?php endif; ?> value="2017">2017</option>
		                    <option <?php if($student->high_school_year == '2018'): ?> selected <?php endif; ?> value="2018">2018</option>
		                    <option <?php if($student->high_school_year == '2019'): ?> selected <?php endif; ?> value="2019">2019</option>
		                    <option <?php if($student->high_school_year == '2020'): ?> selected <?php endif; ?> value="2020">2020</option>
		                    <option <?php if($student->high_school_year == '2021'): ?> selected <?php endif; ?> value="2021">2021</option>
		                </select>
		            </td>
		        </tr>

		        <tbody id="s[1_clarinet_slave" style="display:none;">
		            <tr>
		                <td colspan="2">
		                    <label for="c7_s[1">
		                        <input id="c7_s[1" type="checkbox" name="s[have_access_to_and_experience_playing_on_a_clarinet][]" <?= isset($student->have_access_to_and_experience_playing_on_a_clarinet) ? 'checked' : ''; ?> value="1" /> If you are auditioning on Clarinet please check this box if you have access to AND experience playing on <b>Clarinet in A</b>
		                    </label>
		                </td>
		                </td>
		            </tr>
		        </tbody>
		        <tr>
		            <td colspan="2">
		                <label for="c6_s[1">
		                    <input id="c6_s[1" type="checkbox" <?= isset($student->give_my_name) ? 'checked' : ''; ?> name="s[give_my_name][]" value="1" /> Please feel free to give my name and address to colleges, universities, and music camps</label>
		            </td>
		            </td>
		        </tr>
		    </table>
		</div>

    <?php endforeach; ?>
<?php endif; ?>