<div class="student_fee">
    <table>
        <tr>
            <td colspan="4" class="studentHead">Student <span class="student_number"></span> Information <span class="delete_student" style="display:none;"><a href="#" >DELETE STUDENT</a></span></td>
        </tr>

        <tr>
            <th><b>Band:</b></th>
            <td>
                <select name="s[band][]">
                    <option value="">Select One</option>
                    <option value="2">CBDA High School All-State Honor Band</option>
                    <option value="4">CODA High School All-State Orchestra</option>
                    <option value="5">Either the All-State Band OR the All-State Orchestra</option>
                    <option value="1">CBDA Junior High School All-State Honor Bands (Symphonic, Concert)</option>
                    <option value="3">CBDA/CAJ High School All-State Jazz Band</option>
                    <option value="6">CBDA/CAJ Junior High All State Jazz Band</option>
                </select>
            </td>
        </tr>
        <tr>
            <th>
                <b>Instrument:</b>
            </th>
            <td>
                <select name="s[instrument][]">
                    <option value="">Select One</option>
                    <option value="49">Alto Saxophone</option>
                    <option value="50">Tenor Saxophone</option>
                    <option value="51">Baritone Saxophone</option>
                    <option value="52">Trumpet</option>
                    <option value="53">Trombone</option>
                    <option value="54">Bass Trombone</option>
                    <option value="55">Piano</option>
                    <option value="56">Guitar</option>
                    <option value="57">Bass</option>
                    <option value="58">Drums</option>
                    <option value="59">Vibes</option>
                </select>
            </td>
        </tr>
        <tr>
            <th><b>Student First Name:</b></th>
            <td>
                <span ng-show="(form['s[first_name][]'].$dirty && form['s[first_name][]'].$error.required) || (form.$submitted && form['s[first_name][]'].$error.required)" class='field_error'>Field is requried</span>
                <input ng-if="form.membershipType == 'membership_with_cd'" name="s[first_name][]" type="text" ng-model="form.first_name" required size='26' />
            </td>
        </tr>
        <tr>
            <th>Student Middle Initial:</th>
            <td>
                <input name="s[middle_initial][]" value="" size="26" />
            </td>
        </tr>
        <tr>
            <th><b>Student Last Name:</b></th>
            <td>
                <span ng-show="(form['s[last_name][]'].$dirty && form['s[last_name][]'].$error.required) || (form.$submitted && form['s[last_name][]'].$error.required)" class='field_error'>Field is requried</span>
                <input ng-if="form.membershipType == 'membership_with_cd'" name="s[last_name][]" type="phones" ng-model="form.lastName" required size='26' />
            </td>
        </tr>
        <tr>
            <th>
                <b>Home Phone:</b>
            </th>
            <td>
                <span ng-show="(form['s[home_phone][]'].$dirty && form['s[home_phone][]'].$error.required) || (form.$submitted && form['s[home_phone][]'].$error.required)" class='field_error'>Field is requried</span>
                <input ui-mask='(999)-999-999' ng-if="form.membershipType == 'membership_with_cd'" name="s[home_phone][]" type="text" ng-model="form.homePhone" required size='26' />
            </td>
        </tr>
        <tr>
            <th>
                <b>Address:</b>
            </th>
            <td>
                <span ng-show="(form['s[address][]'].$dirty && form['s[address][]'].$error.required) || (form.$submitted && form['s[address][]'].$error.required)" class='field_error'>Field is requried</span>
                <input ng-if="form.membershipType == 'membership_with_cd'" name="s[address][]" type="text" ng-model="form.addressStudent" required size='26' />
            </td>
        </tr>
        <tr>
            <th>
                <b>City:</b>
            </th>
            <td>
                <span ng-show="(form['s[city][]'].$dirty && form['s[city][]'].$error.required) || (form.$submitted && form['s[city][]'].$error.required)" class='field_error'>Field is requried</span>
                <input ng-if="form.membershipType == 'membership_with_cd'" name="s[city][]" type="text" ng-model="form.cityStudent" required size='26' />
            </td>
        </tr>
        <tr>
            <th><b>State:</b></th>
            <td>
                <input type="hidden" name="state" value="CA">CA</td>
        </tr>
        <tr>
            <th><b>Zip:</b></th>
            <td>
                <span ng-show="(form['s[zip][]'].$dirty && form['s[zip][]'].$error.required) || (form.$submitted && form['s[zip][]'].$error.required)" class='field_error'>Field is requried</span>
                <input ng-if="form.membershipType == 'membership_with_cd'" name="s[zip][]" type="text" ng-model="form.zipStudent" required size='26' />
            </td>
        </tr>
        <tr>
            <th>Parent Email:</th>
            <td>
                <input name="s[parent_email][]" value="" size="26" />
            </td>
        </tr>
        <tr>
            <th>Student Email:</th>
            <td>
                <input name="s[student_email][]" value="" size="26" />
            </td>
        </tr>
        <tr>
            <th><b>Gender:</b></th>
            <td>
                <select name="s[gender][]" _value_="">
                    <option value="">Select One</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </td>
        </tr>
        <tr>
            <th><b>Current Grade:</b></th>
            <td>
                <select name="s[grade_year][]">
                    <option value="">Select One</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
            </td>
        </tr>
        <tr>
            <th><b>Year You Graduate High School:</b></th>
            <td>
                <select name="s[high_school_year][]">
                    <option value="">Select One</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                </select>
            </td>
        </tr>

        <tbody id="s[1_clarinet_slave" style="display:none;">
            <tr>
                <td colspan="2">
                    <label for="c7_s[1">
                        <input id="c7_s[1" type="checkbox" name="have_access_to_and_experience_playing_on_a_clarinet" value="1" /> If you are auditioning on Clarinet please check this box if you have access to AND experience playing on <b>Clarinet in A</b>
                    </label>
                </td>
                </td>
            </tr>
        </tbody>
        <tr>
            <td colspan="2">
                <label for="c6_s[1">
                    <input id="c6_s[1" type="checkbox" name="give_my_name" value="1" /> Please feel free to give my name and address to colleges, universities, and music camps</label>
            </td>
            </td>
        </tr>
    </table>
</div>