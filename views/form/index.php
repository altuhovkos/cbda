<?php 

    $this->title = 'Membership and Student Applications';
    print_r($dataEdit);
?>

<div class="main">
<div class="site_header"></div>
<div class="content">
<center>
<div id="form1">
<a href="membership_application_directions.html" 
target="Membership_Application_Directions">Membership Application 
Directions</a>
&nbsp;|&nbsp;
<a href="recording_instructions.html" 
target="Recording_Instructions">Recording Instructions</a>
&nbsp;|&nbsp;
<a href="membership_dues_and_fees_information.html" 
target="Recording_Instructions">Membership Dues and Fees 
Information</a>
&nbsp;|&nbsp;
<a href="privacy.html" 
target="Recording_Instructions">Privacy Statement</a>

<br /><br />

<h1><b>California Band Directors Association</b></h1>
<h1><b>2015-16 Membership and Student Applications</b></h1>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
<td><br>
<b>"Band Director:  Directions For Completing CBDA Membership and Student   Applications"</b>
<ul><li>1.  Type in all the required information below.</li>
    <li>2.  Click "submit" to save and submit forms electronically to CBDA.  You can sporadically enter data and submit again at a later date and information will be automatically updated. Clicking "submit" also saves your work.</li>
    <li>3.  When you finish typing in all information and click the last "submit" button, forms will appear on your screen for printing in hard copy.</li>
    <li>4.  Print the forms, get the required signatures, and mail with recording(s) and ONE PAYMENT, check, money order or proof of payment via Pay Pal for the total fees (Payable to CBDA) to:  Trish Adams, 2417 N. 11th Ave., Hanford, CA 93230</li>
</ul></td>
  </tr>
</table>
<span>(General Membership and All-State Applicants)</span><br>
<br>

<form method='POST' action='/form<?= (Yii::$app->request->get('edit')) ? '?edit='.Yii::$app->request->get('edit') : false; ?>' name='form' ng-controller='FormController' novalidate>
    <!-- <input type="hidden" name="mod" value="form1"/>
    <input type="hidden" name="method" value="confirm"/>
    <input type="hidden" name="login_key" value=""/> -->
<div class="errorMessage">Please correct errors first</div>
<table>
    <tr>
        <td width="25%">
            <span class="right label"><b>Director First Name:</b></span>
        </td>
        <td width="25%">
            <span class="left">
                <span ng-show="(form.director_first_name.$dirty && form.director_first_name.$error.required) || (form.$submitted && form.director_first_name.$error.required)" class='field_error'>Field is requried</span>
                <input name="director_first_name"  ng-init="user.directorFirstName = '<?= isset($dataEdit->director_first_name) ? $dataEdit->director_first_name : false; ?>'" type="text" ng-model="user.directorFirstName" required size='26' required/>

            </span>
        </td>
        <td width="25%">
            <span class="right label">Director's Main Instrument:</span>
        </td>
        <td width="25%">
            <span class="left"><input tabindex="16" name="directors_main_instrument" value="" size="26"/></span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label">Director Middle Initial:</span>
        </td>
        <td>
            <span class="left"><input ng-model='user.director_middle_initial' ng-init="user.director_middle_initial = '<?= isset($dataEdit->director_middle_initial) ? $dataEdit->director_middle_initial : false; ?>'" tabindex="2" name="director_middle_initial"  size="26"/></span>
        </td>
        <td>
            <input type="hidden" name='new_member' value=''>
            <span class="right"><input tabindex="17" id="c17" type="checkbox" name="new_member" ng-model='user.new_member' ng-init="<?= isset($dataEdit->new_member) ? (($dataEdit->new_member) ? 'user.new_member=true' : 'user.new_member=false') : 'user.new_member=false'; ?>"  /></span>
        </td>
        <td>
            <span class="left label"><label for="c17">New Member Check Here</label></span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label"><b>Director Last Name:</b></span>
        </td>
        <td>

            <span class="left">

                <span  ng-show="(form.director_last_name.$dirty && form.director_last_name.$error.required) || (form.$submitted && form.director_last_name.$error.required)"  class='field_error'>Field is requried</span>
                <input ng-init='user.directorLastName ="<?= isset($dataEdit->director_last_name) ? $dataEdit->director_last_name : false; ?>"'  name="director_last_name" type="text" ng-model="user.directorLastName" required  size='26'/>

            </span>

        </td>
        <td>
            <span class="right label"><b>School Name</b>:</span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.school_name.$dirty && form.school_name.$error.required) || (form.$submitted && form.school_name.$error.required)" class='field_error'>Field is requried</span>
                <input name="school_name" ng-init='user.schoolName ="<?= isset($dataEdit->school_name) ? $dataEdit->school_name : false; ?>"' type="text" ng-model="user.schoolName" required  size='26'/>

            </span>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
            <span class="right label"><b>School Phone:</b></span>
        </td>
        <td>
            <span class="left">
                <span ng-show="(form.school_phone.$dirty && form.school_phone.$error.required) || (form.$submitted && form.school_phone.$error.required)" class='field_error'>Field is requried</span>
                <input name="school_phone" ui-mask="(999) 999-9999" ng-init='user.schoolPhone = "<?= isset($dataEdit->school_phone) ? $dataEdit->school_phone : false; ?>"' type="text" ng-model="user.schoolPhone" required  size='26'/>
            </span>
        </td>
    </tr>

    <tr>
        <td>
            <span class="right label"><b>Home Phone:</b></span>
        </td>
        <td>

            <span class="left">

                <span ng-show="(form.home_phone.$dirty && form.home_phone.$error.required) || (form.$submitted && form.home_phone.$error.required)"  class='field_error'>Field is requried</span>
                <input name="home_phone" ui-mask="(999) 999-9999" ng-init='user.homePhone = "<?= isset($dataEdit->home_phone) ? $dataEdit->home_phone : false; ?>"' type="text" ng-model="user.homePhone" required  size='26'/>

            </span>

        </td>
        <td>
            <span class="right label"><b>Principal's Name:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.principals_name.$dirty && form.principals_name.$error.required) || (form.$submitted && form.principals_name.$error.required)" class='field_error'>Field is requried</span>
                <input name="principals_name" ng-init='user.principalsName ="<?= isset($dataEdit['principals_name']) ? $dataEdit['principals_name'] : false; ?>"' type="text" ng-model="user.principalsName" required tabindex="22" size='26'/>

            </span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label"><b>Address:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.address.$dirty && form.address.$error.required) || (form.$submitted && form.address.$error.required)" class='field_error'>Field is requried</span>
                <input name="address" type="text" ng-init='user.address ="<?= isset($dataEdit->address) ? $dataEdit->address : false; ?>"' ng-model="user.address" required tabindex="22" size='26'/>

            </span>
        </td>
        <td>

            <input type="hidden" name='past_president' value=''>
            <span class="right"><input tabindex="17" id="c17" type="checkbox" name="past_president" ng-model='user.past_president' ng-init="<?= isset($dataEdit->past_president) ? (($dataEdit->past_president) ? 'user.past_president=true' : 'user.past_president=false') : 'user.past_president=false'; ?>"  /></span>
        </td>
        <td>
            <span class="left label"><label for="c23">Past President (NO FEE) Check Here</label></span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label"><b>City:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.city.$dirty && form.city.$error.required) || (form.$submitted && form.city.$error.required)" class='field_error'>Field is requried</span>
                <input ng-init='user.city ="<?= isset($dataEdit->city) ? $dataEdit->city : false; ?>"'  name="city" type="text" ng-model="user.city" required tabindex="22" size='26'/>

            </span>
        </td>
        <td>
            <span class="right label">Fax Number:</span>
        </td>
        <td>
            <span class="left"><input tabindex="25" name="fax" ui-mask="999-999-999" ng-model='faxNumber' ng-init='faxNumber="<?= isset($dataEdit->fax) ? $dataEdit->fax : false; ?>"' type='text' value=""/></span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label"><b>State:</b></span>
        </td>
        <td>
            <span class="left sel"><span class="field_error" style='display: none;'>Field is requried</span><select required tabindex="10" style="display:inline;" name="state">
            <option <?php if(isset($dataEdit->state) && $dataEdit->state == 'AL'): ?> selected <?php endif; ?> value="AL">AL</option>
            <option <?php if(isset($dataEdit->state) && $dataEdit->state == 'AK'): ?> selected <?php endif; ?> value="AK">AK</option>
            <option <?php if(isset($dataEdit->state) && $dataEdit->state == 'AZ'): ?> selected <?php endif; ?> value="AZ">AZ</option>
            <option <?php if(isset($dataEdit->state) && $dataEdit->state == 'AR'): ?> selected <?php endif; ?> value="AR">AR</option>
            <option <?php if(isset($dataEdit->state) && $dataEdit->state == 'CA'): ?> selected <?php endif; ?> value="CA">CA</option>
            <option <?php if(isset($dataEdit->state) && $dataEdit->state == 'CO'): ?> selected <?php endif; ?> value="CO">CO</option>
            <option <?php if(isset($dataEdit->state) && $dataEdit->state == 'CT'): ?> selected <?php endif; ?> value="CT">CT</option>
            <option <?php if(isset($dataEdit->state) && $dataEdit->state == 'DE'): ?> selected <?php endif; ?> value="DE">DE</option>
            <option <?php if(isset($dataEdit->state) && $dataEdit->state == 'FL'): ?> selected <?php endif; ?> value="FL">FL</option>
            </select></span>



        </td>
        <td>
            <span class="right label"><b>School Address:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.school_address.$dirty && form.school_address.$error.required) || (form.$submitted && form.school_address.$error.required)"  class='field_error'>Field is requried</span>
                <input ng-init='user.schoolAddress ="<?= isset($dataEdit->school_address) ? $dataEdit->school_address : false; ?>"'  name="school_address" type="text" ng-model="user.schoolAddress" required tabindex="22" size='26'/>

            </span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label"><b>Zip:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.zip.$dirty && form.zip.$error.required) || (form.$submitted && form.zip.$error.required)"  class='field_error'>Field is requried</span>
                <input name="zip" ng-init='user.zip ="<?= isset($dataEdit->zip) ? $dataEdit->zip : false; ?>"' type="text" ng-model="user.zip" required tabindex="22" size='26'/>

            </span>
        </td>
        <td>
            <span class="right label"><b>City:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.school_city.$dirty && form.school_city.$error.required) || (form.$submitted && form.school_city.$error.required)" class='field_error'>Field is requried</span>
                <input name="school_city" ng-init='user.schoolCity ="<?= isset($dataEdit->school_city) ? $dataEdit->school_city : false; ?>"' type="text" ng-model="user.schoolCity" required tabindex="22" size='26'/>

            </span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label"><b>Email Address:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.email.$dirty && form.email.$error.required) || (form.$submitted && form.email.$error.required )" class='field_error'>Field is requried and must be email</span>
                <input name="email" type="text" ng-init='user.email ="<?= isset($dataEdit->email) ? $dataEdit->email : false; ?>"' ng-model="user.email" required tabindex="22" size='26'/>

            </span>
        </td>
        <td>
            <span class="right label"><b>State:</b></span>
        </td>
        <td>
            <span class="left sel"><span class="field_error" style='display: none;'>Field is requried</span><select tabindex="29" name="school_state" >
            <option <?php if( isset($dataEdit->school_state) &&  $dataEdit->school_state == 'ALASKA'): ?> selected <?php endif; ?> value="ALASKA">ALASKA</option>
            <option <?php if( isset($dataEdit->school_state) &&  $dataEdit->school_state == 'ARIZONA'): ?> selected <?php endif; ?> value="ARIZONA">ARIZONA</option>
            <option <?php if( isset($dataEdit->school_state) &&  $dataEdit->school_state == 'ARKANSAS'): ?> selected <?php endif; ?> value="ARKANSAS">ARKANSAS</option>
            <option <?php if( isset($dataEdit->school_state) &&  $dataEdit->school_state == 'CALIFORNIA'): ?> selected <?php endif; ?> value="CALIFORNIA">CALIFORNIA</option>
            <option <?php if( isset($dataEdit->school_state) &&  $dataEdit->school_state == 'COLORADO'): ?> selected <?php endif; ?> value="COLORADO">COLORADO</option>
            <option <?php if( isset($dataEdit->school_state) &&  $dataEdit->school_state == 'CONNECTICUT'): ?> selected <?php endif; ?> value="CONNECTICUT">CONNECTICUT</option>
            <option <?php if( isset($dataEdit->school_state) &&  $dataEdit->school_state == 'DELAWARE'): ?> selected <?php endif; ?> value="DELAWARE">DELAWARE</option>
            </select></span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label"><b>Confirm Email Address:</b></span>
        </td>
        <td>
            <span class="left">
                <span ng-show="(form.confirm_email.$dirty && form.confirm_email.$error.required) || (form.$submitted && form.confirm_email.$error.required)" class='field_error'>Field is requried and must be email</span>
                <span ng-show="form.confirm_email.$error.passwordVerify" class='field_error'>Email is not confirm</span>
                <input name="confirm_email" ng-init='user.confirm_email ="<?= isset($dataEdit->email) ? $dataEdit->email : false; ?>"'  type="email" data-password-verify="user.email" ng-model="user.confirm_email" required tabindex="15" size='26'/>
            </span>
        </td>
        <td>
            <span class="right label"><b>Zip:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.school_zip.$dirty && form.school_zip.$error.required) || (form.$submitted && form.school_zip.$error.required)" class='field_error'>Field is requried</span>
                <input name="school_zip" type="text" ng-init='user.schoolZip ="<?= isset($dataEdit->school_zip) ? $dataEdit->school_zip : false; ?>"' ng-model="user.schoolZip" required tabindex="22" size='26'/>

            </span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label"><b>Password:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.password.$dirty && form.password.$error.required) || (form.$submitted && form.password.$error.required)" class='field_error'>Field is requried</span>
                <input name="password" type="password" ng-init='user.password ="<?= isset($dataEdit->password) ? $dataEdit->password : false; ?>"' ng-model="user.password" required tabindex="22" size='26'/>

            </span>
        </td>
        <td style="vertical-align:top;"> 
            <input type="hidden" name='i_ll_help_with_auditions' value=''>
            <span class="right"><input tabindex="31" id="c31" <?= ( isset($dataEdit->i_ll_help_with_auditions) && !empty($dataEdit->i_ll_help_with_auditions) ) ? 'checked' : '' ?> type="checkbox" name="i_ll_help_with_auditions" value="1" /></span>
        </td>
        <td>
            <span class="left label" style="position:relative;">
            <label for="c31" style="position:absolute;width:210px;top:-1.4em;">I'll help with live auditions at 11:00 am on Thursday, Feb 11</label></span>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><small>(Use combination of upper and lower case letters and numbers for security purposes)</small></td>
        <td>

            <input type="hidden" name='i_m_playing_in_reading_band' value=''>
            <span class="right"><input tabindex="31" id="c31" <?= ( isset($dataEdit->i_m_playing_in_reading_band) && !empty($dataEdit->i_m_playing_in_reading_band) ) ? 'checked' : '' ?> type="checkbox" name="i_m_playing_in_reading_band" value="1" /></span>

        </td>
        <td>
            <span class="left label"><label for="c32">I'm playing in reading band</label></span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="right label"><b>Retype-Password:</b></span>
        </td>
        <td>
            <span class="left">

                <span ng-show="(form.re_password.$dirty && form.re_password.$error.required) || (form.$submitted && form.re_password.$error.required)" class='field_error'>Field is requried</span>
                <span ng-show="form.re_password.$error.passwordVerify" class='field_error'>Pasword is not confirm</span>
                <input name="re_password" type="password" ng-init='user.rePassword ="<?= isset($dataEdit->password) ? $dataEdit->password : false; ?>"' data-password-verify="user.password" ng-model="user.rePassword" required tabindex="15" size='26'/>

            </span>
        </td>
        <td style="vertical-align:top;">
            <input type="hidden" name='do_not_publish_my_home_contact' value=''>
            <span class="right"><input tabindex="31" id="c31" <?= ( isset($dataEdit->do_not_publish_my_home_contact) && !empty($dataEdit->do_not_publish_my_home_contact) ) ? 'checked' : '' ?> type="checkbox" name="do_not_publish_my_home_contact" value="1" /></span>

        </td>
        <td>
            <span class="left label"><label for="c33">Do not publish my home contact information in the Directory</label></span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <small>(The password creates your online account and will allow you to add/delete/edit data at a later time.)</small>
        </td>
        <td style="vertical-align:top;">
            <input type="hidden" name='do_not_share_my_email' value=''>
            <span class="right"><input tabindex="31" id="c31" <?= ( isset($dataEdit->do_not_share_my_email) && !empty($dataEdit->do_not_share_my_email) ) ? 'checked' : '' ?> type="checkbox" name="do_not_share_my_email" value="1" /></span>
        </td>
        <td>
            <span class="left label"><label for="c34">Do not share my email with third parties. (colleges, universities, music camps)</label></span>
        </td>
    </tr>
    <tr>
        <td colspan="4"><h3>Payment for Membership Fees</h3></td>
    </tr>
    <?php $l = ($studentsFees) ? 's' : false; ?>
    <tr>
        <td colspan="4">
            <div style="font-size:16px;font-weight:bold;">

                <input tabindex="33" id="membership_type1" type="radio" name="membership_type"  value="membership_only" ng-model="form.membershipType"/><label for="membership_type1">Membership Only</label>
                <input tabindex="34" id="membership_type2" type="radio" name="membership_type"  value="membership_with_cd" ng-model="form.membershipType" ng-init="form.membershipType='<?= (isset($dataEdit->membership_type) && !empty($dataEdit->membership_type)) ? $dataEdit->membership_type : ''; ?>'"  /><label for="membership_type2">Membership with CD submission (only CD's are accepted)</label>
                <br /><br />
            </div>
            <table class="fees" id="fees_table" ng-show="form.membershipType == 'membership_only' || form.membershipType == 'membership_with_cd'" >
                <tr>
                    <td colspan="3" valign="top"><h4><b>Director's Annual Membership Dues:</b></h4> (CBDA Dues Only)</td>
                    <td class="border_left" rowspan="8"></td>
                    <td class="border_left" rowspan="8" align="center" valign="top">
                        Membership from<br />
                        Dec. 15 - Nov. 16<br />
                        (Required for<br />
                        Audition Submission)
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">Dues are $50.00 unless otherwise noted below</td>
                </tr>

                 

                <tr>
                    <td></td>
                    <td><label for="c1"><input tabindex="35" id="c1" type="radio" name="annual_dues" ng-init="<?= (isset($dataEdit->new_member) && $dataEdit->new_member == 10)  ? 'user.new_member=true' : 'user.new_member=false' ; ?>"  rel="50.00" ng-model='form.totalPrice.elem'  value="10"/> Active Elementary School Band Director</label></td>
                    <td><label for="c2"><input tabindex="35" id="c2" type="radio" name="annual_dues" rel="50.00" ng-model='form.totalPrice.elem' ng-init="<?= (isset($dataEdit->annual_dues) && $dataEdit->annual_dues)  ? 'form.totalPrice.elem='.$dataEdit->annual_dues : '' ; ?>"  value="20"/> Associate Member</label></td>
                </tr>
                <tr>                    +
                    <td></td>
                    <td><label for="c3"><input tabindex="35" id="c3" type="radio" name="annual_dues" rel="50.00" ng-model='form.totalPrice.elem'  value="30"/> Active Middle/Junior High Band Director</label></td>
                    <td><label for="c4"><input tabindex="35" id="c4" type="radio" name="annual_dues" rel="25.00" ng-model='form.totalPrice.elem' value="25" /> Retired Band Director ($25.00)</label></td>
                </tr>
                <tr>
                    <td></td>
                    <td><label for="c5"><input tabindex="35" id="c5" type="radio" name="annual_dues" rel="50.00" ng-model='form.totalPrice.elem' value="550" /> Active High School Band Director</label></td>
                    <td><label for="c6"><input tabindex="35" id="c6" type="radio" name="annual_dues" rel="10.00" ng-model='form.totalPrice.elem' value="100" /> College Student ($10.00)</label></td>
                </tr>
                <tr>
                    <td></td>
                    <td><label for="c7"><input tabindex="35" id="c7" type="radio" name="annual_dues" rel="50.00" ng-model='form.totalPrice.elem' value="600" /> Active College Band Director</label></td>
                    <td><label for="c8"><input tabindex="35" id="c8" type="radio" name="annual_dues" rel="00.00" ng-model='form.totalPrice.elem' value="140" /> Past President/LifeMember ($0.00)</label></td>
                </tr>
                <tr>
                    <td></td>
                    <td><label for="c9"><input tabindex="35" id="c9" type="radio" name="annual_dues" rel="50.00" ng-model='form.totalPrice.elem' value="75" /> Music Administrator</label></td>
                    <td><label for="c10"><input tabindex="35" id="c10" type="radio" name="annual_dues" rel="50.00" ng-model='form.totalPrice.elem' value="70" /> Associate Institutional (Music Industry)</label></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"><label for="c11"><input tabindex="35" id="c11" type="radio" name="annual_dues" rel="00.00" ng-model='form.totalPrice.elem' value="0" /> Already a Current Member(if adding an additional school - <i>subject to verification</i>)</label></td>
                </tr>
                <tr>
                    <td class="border_bottom"></td>
                    <td class="border_bottom"></td>
                    <td class="border_bottom"></td>
                    <td class="border_left border_bottom">$<input name="annual_dues_total" size="6" class="disabled" value='{{form.totalPrice.elem}}' disabled/></td>
                    <td class="border_left border_bottom"><b>$50</b></td>
                </tr>

                <tr>
                    <td colspan="3" valign="top"><h4><b>Conference Fee:</b></h4></td>
                    <td class="border_left"></td>
                    <td class="border_left"></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"><input tabindex="36" id="c20" ng-model='form.registrationType' type="radio" name="pre_registration_member" <?= (isset($dataEdit->pre_registration_member) && $dataEdit->pre_registration_member == 85)  ? 'ng-init="form.registrationType=85"' : '' ; ?> name="pre_registration_member" ng-model='form.registrationType' value="85" /><label for="c20"> Member Registration fees: Active/$85; Retired/$35; Coillege Student/$25</label></td>
                    <td class="border_left">$<input name="pre_registration" size="6" value='{{form.registrationType}}' class="disabled" disabled/></td>
                    <td class="border_left"><b>$<span id="pre_registration_fee_value"></span></b><br />Regular Members</td>
                </tr><br /><tr>
                    <td></td>
                    <td colspan="2"><input tabindex="36" value='135' id="c20" type="radio" <?= (isset($dataEdit->pre_registration_member) && $dataEdit->pre_registration_member == 135)  ? 'ng-init="form.registrationType=135"' : '' ; ?> name="pre_registration_member" ng-model='form.registrationType' /><label for="c20"> Non-member registration fee: $135</label></td>
                    <td class="border_left"></td>
                    <td class="border_left"><b>$135</b><br />Non-Members</td>
                </tr>
                <tr>
                    <td class="border_bottom"></td>
                    <td class="border_bottom" colspan="2">
                        <input type="hidden" name='banquet' value=''>
                        <input tabindex="37" ng-model="checked" id="c21" type="checkbox" name="banquet"  ng-init='checked=<?= (isset($dataEdit->banquet) && $dataEdit->banquet )  ? 'true' : '' ; ?>'  value="1" /><label for="c21"> CBDA Banquet</label>
                        <input tabindex="38" name="banquet_attendings_number" value="" ng-readonly="!checked" size="5" ng-init='form.numOfBanquest=<?= (isset($dataEdit->banquet_attendings_number) && $dataEdit->banquet_attendings_number )  ? $dataEdit->banquet_attendings_number : 0 ; ?>' ng-model='form.numOfBanquest' class="disabled" readonly="readonly" /> attending @ $45.00 per person
                    </td>
                    <td class="border_left border_bottom">$<input name="banquet_fee" size="6"  class="disabled" disabled ng-if='checked' value='{{ form.numOfBanquest ? 45 * form.numOfBanquest : 0 }}' /></td>
                    <td class="border_left border_bottom">Banquet $45.00 ea.<br />(optional)</td>
                </tr>
            <tbody id="membership_type_membership_with_cd" ng-show="form.membershipType == 'membership_with_cd'">
                <tr>
                    <td class="border_bottom" colspan="3"><h4><b>School Fee:</b></h4> (required of all schools submitting All-State audition CD's)</td>
                    <td class="border_left border_bottom">$<input name="school_fee"  value="55.00"  size="6" class="disabled" disabled/></td>
                    <td class="border_left border_bottom">(Required for Audition Submissions)<br /><b>$55.00</b></td>
                </tr>

                <tr>
                    <td class="border_left" colspan="3" valign="top"><h4><b>Student Fees:</b></h4></td>
                    <td class="border_left"></td>
                    <td class="border_left"></td>
                </tr>

                <tr>
                    <td class="border_bottom" colspan="3">
                        <div class="student_fees">
                            <?php echo $this->render('student'.$l, ['studentsFees' => $studentsFees]);  ?>
                        </div>
                        <div class="add_student"><a href="#" >ADD STUDENT</a><div>
                    </td>
                    <td class="border_left" valign="top">$<input name="student_fee" value='55' ng-model='form.totalStudentFee' size="6" class="disabled" disabled/></td>
                    <td class="border_left" valign="top">
                        (Required for Auditions)<br />
                        2 Fees Required when<br />
                        same student applies for<br />
                        Concert and Jazz Band
                    </td>
                </tr>
            </tbody>
                <tr>
                    <td class="border_bottom border_top" colspan="3"><h4><b>Donations to the Don Schmeer Scholarship Trust Fund</b></h4></td>
                    <td class="border_bottom border_top border_left">$<input name="donat" <?= !empty($dataEdit->donat) ? "ng-init='donat=".$dataEdit->donat."'" : '' ?> ng-model='donat' value="" size="6"/></td>
                    <td class="border_bottom border_top border_left">Enter Donation</td>
                </tr>

                <tr>
                    <td colspan="5">
                        <h4><b>TOTAL</b></h4>
                        (Auditions are not accepted unless director is current CBDA member)
                        $<input name="fee_total" value="{{ (form.totalPrice.elem  -- form.registrationType -- totalStudentFee -- donat -- (form.numOfBanquest ? 45 * form.numOfBanquest : 0)) + 55 + form.totalStudentFee}}" size="6" class="disabled" disabled/>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="right">
                        One Payment Please<br />
                        <b><i>All fees non-refundable</i></b>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4" id="payment_options" style="display:none;">
            Payment Options:
            <input id="payment_options1" type="radio" name="payment_options" value="check_number"/><label for="payment_options1">Payment by Check</label>
            <input id="payment_options2" type="radio" name="payment_options" value="money_order"/><label for="payment_options2">Money Order</label>
            <input id="payment_options3" type="radio" name="payment_options" value="credit_card" checked/><label for="payment_options3">Credit card (PayPal)</label>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="center">
            {{}}
            <h3 align='center' style="color: red;" ng-show='form.$invalid'>Form is not valid, but you can save</h3>
            <h3 align='center' style="color: red;" ng-show='form.re_password.$error.passwordVerify'>Pleace, writing passwords fields for save form</h3>
            <input class="button" type="submit" name="methodSub" ng-model='saveForm' ng-disabled="form.re_password.$error.passwordVerify" value="SAVE"/>
            <input class="button" type="submit" ng-disabled="form.$invalid" value="SUBMIT"/>
            <br/><br/>
        </td>
    </tr>
</table>
</form>
</div>
</center>
