<div class="main">
	<div class="site_header"></div>
	<div class="content">
		<center>

<div id="form1">

	<font size="5">Congratulations!</font>
	<p>Your information has been successfully sent to CBDA.<br />
      <font size="3"><b>Please write down Application ID and password and save for future entries and edits.</b></font><br>
      <br />
      Application ID: <b><?= $data->id ?></b><br />
      Password: <b><?= $data->password ?></b><br>
      <br>
      A confirmation message has been sent to the email address you provided on the on-line registration form.<br>
      The email message contains your Application ID, password and a PDF of your application.&nbsp;      <br>
      <br>
      Be sure to download and print your applications and mail the completed applications with Payment or PayPal receipt to:<br>
      <br>
      <font size="4"><b>
    Trish Adams <br>
	2417 N. 11th Ave.<br>
    Hanford, CA&nbsp; 93230</b></font></p>
	<table width="75%"  border="1" cellpadding="8" cellspacing="2" bordercolor="#000000">
      <tr>
        <td valign="top" style="font-size:18px"><b>To download and print the pdf file of your completed forms for signatures and mailing <a href="print_order.html?login_key=63-8642a822235f0addc2111d6e9cd8a4f1">CLICK HERE</a><b> <br><br><br><br></td>
        <td valign="top"><p><strong>Be sure the completed packet includes:</strong><br>
          1.&nbsp; All CD recordings (if applicable)<br>
          2.&nbsp; Completed Student Application forms (if applicable)<br>
          3.&nbsp; Director Membership/School Application form<br>
          4.&nbsp; ONE CHECK, MONEY ORDER OR PROOF WITH PAYPAL RECEIPT PAYABLE TO CBDA (IF YOU ARE PAYING WITH CREDIT CARD)<br>
          5.&nbsp; Band Director MUST send ONE PAYMENT to cover all fees (No Purchase Orders).<br>
          6.&nbsp; It is suggested that the band director mail application and recording by Delivery Confirmation. Please DO NOT send as "Signature Required."</p>        </td>
      </tr>
      <tr>
        <td valign="top">Click Here to<br>          <a href="/form/edit">Edit Saved Form </a><br /></td>
        <td valign="top">Make changes or additions to your application.</td>
      </tr>
      <tr>
        <td colspan="2" valign="top"><p>All forms and applications posted on this site are in <b>Portable Document Format</b> (PDF). Once downloaded, you will need <b>Adobe Reader</b> to view and print them. Most likely you already have it on your computer. However, if you have trouble opening the files, you will need to get Adobe Reader and install it on your computer. Fortunately, it is <b>FREE</b> and can be downloaded by clicking on the following link. </p>
        <center>
            <a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank"><img src="http://cbda.org/images/getacro.gif" border="0"></a>
          </center>          <br>
  A little helpful advice... <B>BE PATIENT!</B> Large PDF files take time to download.</td>
      </tr>
    </table>
	 <font size="4"><strong><br>
    All CD recording application packets must be postmarked by December 1, 2015. </strong></font>
<br><br>
<a href="membership_application_directions.html" 
target="Membership_Application_Directions">Membership Application 
Directions</a>
&nbsp;|&nbsp;
<a href="recording_instructions.html" 
target="Recording_Instructions">Recording Instructions</a>
&nbsp;|&nbsp;
<a href="membership_dues_and_fees_information.html" 
target="Recording_Instructions">Membership Dues and Fees 
Information</a>

<br /><br />

  </div>

</center>
	</div>
</div>