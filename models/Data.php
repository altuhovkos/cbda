<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data".
 *
 * @property integer $id
 * @property string $director_first_name
 * @property string $director_middle_initial
 * @property string $director_last_name
 * @property string $home_phone
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $email
 * @property string $password
 * @property string $directors_main_instrument
 * @property integer $new_member
 * @property string $school_name
 * @property string $school_phone
 * @property string $principals_name
 * @property integer $past_president
 * @property string $fax
 * @property string $school_address
 * @property string $school_city
 * @property string $school_state
 * @property string $school_zip
 * @property integer $i_ll_help_with_auditions
 * @property integer $i_m_playing_in_reading_band
 * @property integer $do_not_publish_my_home_contact
 * @property integer $do_not_share_my_email
 * @property integer $membership_type
 * @property integer $annual_dues
 * @property integer $pre_registration_member
 * @property integer $pre_registration_non_member
 * @property integer $banquet
 * @property integer $banquet_attendings_number
 * @property string $payment_options
 * @property integer $payment_status
 * @property string $status
 *
 * @property Students[] $students
 */
class Data extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * @inheritdoc
     */

    public function saveData(){
        $this->save();
        return $this->id;
    }

    public function rules()
    {
        return [
            [['address', 'city', 'password', 'directors_main_instrument','membership_type', 'donat'], 'string'],
            [['new_member', 'past_president', 'i_ll_help_with_auditions', 'i_m_playing_in_reading_band', 'do_not_publish_my_home_contact', 'do_not_share_my_email', 'annual_dues', 'pre_registration_member', 'pre_registration_non_member', 'banquet', 'banquet_attendings_number', 'payment_status'], 'string'],
            [['director_first_name', 'director_middle_initial', 'director_last_name', 'home_phone', 'zip', 'email', 'school_name', 'school_phone', 'principals_name', 'fax', 'school_address', 'school_city', 'payment_options', 'status'], 'string', 'max' => 255],
            [['state', 'school_state'], 'string'],
            [['school_zip'], 'string', 'max' => 26]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'director_first_name' => 'Director First Name',
            'director_middle_initial' => 'Director Middle Initial',
            'director_last_name' => 'Director Last Name',
            'home_phone' => 'Home Phone',
            'address' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'Zip',
            'email' => 'Email',
            'password' => 'Password',
            'directors_main_instrument' => 'Directors Main Instrument',
            'new_member' => 'New Member',
            'school_name' => 'School Name',
            'school_phone' => 'School Phone',
            'principals_name' => 'Principals Name',
            'past_president' => 'Past President',
            'fax' => 'Fax',
            'school_address' => 'School Address',
            'school_city' => 'School City',
            'school_state' => 'School State',
            'school_zip' => 'School Zip',
            'i_ll_help_with_auditions' => 'I Ll Help With Auditions',
            'i_m_playing_in_reading_band' => 'I M Playing In Reading Band',
            'do_not_publish_my_home_contact' => 'Do Not Publish My Home Contact',
            'do_not_share_my_email' => 'Do Not Share My Email',
            'membership_type' => 'Membership Type',
            'annual_dues' => 'Annual Dues',
            'pre_registration_member' => 'Pre Registration Member',
            'pre_registration_non_member' => 'Pre Registration Non Member',
            'banquet' => 'Banquet',
            'banquet_attendings_number' => 'Banquet Attendings Number',
            'payment_options' => 'Payment Options',
            'payment_status' => 'Payment Status',
            'status' => 'Status',
            'donat' => 'Donat'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Students::className(), ['data_id' => 'id']);
    }
}
