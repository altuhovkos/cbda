<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "students".
 *
 * @property integer $id
 * @property integer $data_id
 * @property string $band
 * @property string $instrument
 * @property string $first_name
 * @property string $middle_initial
 * @property string $last_name
 * @property string $home_phone
 * @property string $address
 * @property string $city
 * @property string $zip
 * @property string $parent_email
 * @property string $student_email
 * @property integer $gender
 * @property integer $grade_year
 * @property integer $high_school_year
 *
 * @property Data $data
 */
class Students extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'students';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_id', 'grade_year', 'high_school_year'], 'integer'],
            [['band', 'gender', 'instrument', 'first_name', 'middle_initial', 'last_name', 'home_phone', 'address', 'city', 'parent_email', 'student_email', 'give_my_name', 'have_access_to_and_experience_playing_on_a_clarinet'], 'string', 'max' => 255],
            [['zip'], 'string', 'max' => 26]
        ];
    }

    /**
     * @inheritdoc
     */

    public function prepeareData($data){
        $tmp = false;
        foreach ($data as $key_s => $value_s) 
            foreach ($value_s as $key_v => $value_v) 
                $tmp[$key_v][$key_s] = $value_v;

        return $tmp;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_id' => 'Data ID',
            'band' => 'Band',
            'instrument' => 'Instrument',
            'first_name' => 'First Name',
            'middle_initial' => 'Middle Initial',
            'last_name' => 'Last Name',
            'home_phone' => 'Home Phone',
            'address' => 'Address',
            'city' => 'City',
            'zip' => 'Zip',
            'parent_email' => 'Parent Email',
            'student_email' => 'Student Email',
            'gender' => 'Gender',
            'grade_year' => 'Grade Year',
            'high_school_year' => 'High School Year',
            'give_my_name' => 'Attr',
            'have_access_to_and_experience_playing_on_a_clarinet' => 'attr2'
        ];
    }

    public function saveData($data, $data_id){
        foreach ($data as $key => $value) {

            if ( empty($value['id']) && ( isset($value['ifRemoveId']) && !empty($value['ifRemoveId']) ) ) {
                $st = Students::find()->where(['id' => $value['ifRemoveId']])->one();
                $st->delete();
                continue;
            }

            if (isset($value['id']) && !empty($value['id'])) {
                $st = Students::find()->where(['id' => $value['id']])->one();
            }
            else{
                $st = new Students;
            }
            
            $st->attributes = $value;

            $st->data_id = $data_id;

            $st->save();

        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getData()
    {
        return $this->hasOne(Data::className(), ['id' => 'data_id']);
    }
}
