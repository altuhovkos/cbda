<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Data;

/**
 * CrudData represents the model behind the search form about `app\models\Data`.
 */
class CrudData extends Data
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'new_member', 'past_president', 'i_ll_help_with_auditions', 'i_m_playing_in_reading_band', 'do_not_publish_my_home_contact', 'do_not_share_my_email', 'membership_type', 'annual_dues', 'pre_registration_member', 'pre_registration_non_member', 'banquet', 'banquet_attendings_number', 'payment_status'], 'integer'],
            [['director_first_name', 'director_middle_initial', 'director_last_name', 'home_phone', 'address', 'city', 'state', 'zip', 'email', 'password', 'directors_main_instrument', 'school_name', 'school_phone', 'principals_name', 'fax', 'school_address', 'school_city', 'school_state', 'school_zip', 'payment_options', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Data::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 2,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'new_member' => $this->new_member,
            'past_president' => $this->past_president,
            'i_ll_help_with_auditions' => $this->i_ll_help_with_auditions,
            'i_m_playing_in_reading_band' => $this->i_m_playing_in_reading_band,
            'do_not_publish_my_home_contact' => $this->do_not_publish_my_home_contact,
            'do_not_share_my_email' => $this->do_not_share_my_email,
            'membership_type' => $this->membership_type,
            'annual_dues' => $this->annual_dues,
            'pre_registration_member' => $this->pre_registration_member,
            'pre_registration_non_member' => $this->pre_registration_non_member,
            'banquet' => $this->banquet,
            'banquet_attendings_number' => $this->banquet_attendings_number,
            'payment_status' => $this->payment_status,
        ]);

        $query->andFilterWhere(['like', 'director_first_name', $this->director_first_name])
            ->andFilterWhere(['like', 'director_middle_initial', $this->director_middle_initial])
            ->andFilterWhere(['like', 'director_last_name', $this->director_last_name])
            ->andFilterWhere(['like', 'home_phone', $this->home_phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'zip', $this->zip])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'directors_main_instrument', $this->directors_main_instrument])
            ->andFilterWhere(['like', 'school_name', $this->school_name])
            ->andFilterWhere(['like', 'school_phone', $this->school_phone])
            ->andFilterWhere(['like', 'principals_name', $this->principals_name])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'school_address', $this->school_address])
            ->andFilterWhere(['like', 'school_city', $this->school_city])
            ->andFilterWhere(['like', 'school_state', $this->school_state])
            ->andFilterWhere(['like', 'school_zip', $this->school_zip])
            ->andFilterWhere(['like', 'payment_options', $this->payment_options])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
