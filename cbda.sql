-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 20 2015 г., 16:38
-- Версия сервера: 5.6.25-0ubuntu0.15.04.1
-- Версия PHP: 5.6.4-4ubuntu6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `cbda`
--

-- --------------------------------------------------------

--
-- Структура таблицы `data`
--

CREATE TABLE IF NOT EXISTS `data` (
`id` int(11) NOT NULL,
  `director_first_name` varchar(255) DEFAULT NULL,
  `director_middle_initial` varchar(255) DEFAULT NULL,
  `director_last_name` varchar(255) DEFAULT NULL,
  `home_phone` varchar(255) DEFAULT NULL,
  `address` text,
  `city` text,
  `state` varchar(2) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` text,
  `directors_main_instrument` text,
  `new_member` int(11) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_phone` varchar(255) DEFAULT NULL,
  `principals_name` varchar(255) DEFAULT NULL,
  `past_president` int(11) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `school_address` varchar(255) DEFAULT NULL,
  `school_city` varchar(255) DEFAULT NULL,
  `school_state` varchar(2) DEFAULT NULL,
  `school_zip` varchar(26) DEFAULT NULL,
  `i_ll_help_with_auditions` int(11) DEFAULT NULL,
  `i_m_playing_in_reading_band` int(11) DEFAULT NULL,
  `do_not_publish_my_home_contact` int(11) DEFAULT NULL,
  `do_not_share_my_email` int(11) DEFAULT NULL,
  `membership_type` int(11) DEFAULT NULL,
  `annual_dues` int(11) DEFAULT NULL,
  `pre_registration_member` int(11) DEFAULT NULL,
  `pre_registration_non_member` int(11) DEFAULT NULL,
  `banquet` int(11) DEFAULT NULL,
  `banquet_attendings_number` int(11) DEFAULT NULL,
  `payment_options` varchar(255) DEFAULT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT 'save'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `data`
--

INSERT INTO `data` (`id`, `director_first_name`, `director_middle_initial`, `director_last_name`, `home_phone`, `address`, `city`, `state`, `zip`, `email`, `password`, `directors_main_instrument`, `new_member`, `school_name`, `school_phone`, `principals_name`, `past_president`, `fax`, `school_address`, `school_city`, `school_state`, `school_zip`, `i_ll_help_with_auditions`, `i_m_playing_in_reading_band`, `do_not_publish_my_home_contact`, `do_not_share_my_email`, `membership_type`, `annual_dues`, `pre_registration_member`, `pre_registration_non_member`, `banquet`, `banquet_attendings_number`, `payment_options`, `payment_status`, `status`) VALUES
(18, 'esrgserg', '', '', '', '', '', 'CA', '', '', '', '', NULL, '', '', '', NULL, NULL, '', '', 'AL', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'credit_card', 0, 'save'),
(19, 'awefawef', '', 'drthdrthrdt', '324', 'awefawe', 'пвапва', 'CA', 'awefawef', 'konstantin255@gmail.com', '123456', 'awefawef', NULL, 'awefawef', '', 'awefawef', NULL, NULL, 'segsergser', 'gserrgserg', 'AR', 'sergser', NULL, NULL, NULL, 1, 0, 140, 85, NULL, 1, 55, 'credit_card', 0, 'save'),
(20, 'awefawef', '', 'drthdrthrdt', '324', 'awefawe', 'пвапва', 'CA', 'awefawef', 'konstantin255@gmail.com', '123456', 'awefawef', NULL, 'awefawef', '', 'awefawef', NULL, NULL, 'segsergser', 'gserrgserg', 'AR', 'sergser', NULL, NULL, NULL, 1, 0, 140, 85, NULL, 1, 55, 'credit_card', 0, 'save');

-- --------------------------------------------------------

--
-- Структура таблицы `students`
--

CREATE TABLE IF NOT EXISTS `students` (
`id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `band` varchar(255) DEFAULT NULL,
  `instrument` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_initial` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `home_phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zip` varchar(26) DEFAULT NULL,
  `parent_email` varchar(255) DEFAULT NULL,
  `student_email` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `grade_year` int(11) DEFAULT NULL,
  `high_school_year` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `students`
--

INSERT INTO `students` (`id`, `data_id`, `band`, `instrument`, `first_name`, `middle_initial`, `last_name`, `home_phone`, `address`, `city`, `zip`, `parent_email`, `student_email`, `gender`, `grade_year`, `high_school_year`) VALUES
(13, 1, '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL),
(14, 1, '4', '52', '345345', '', '345345', 'drhdrth', 'drthdrth', 'drth', 'drthdrth', '', '', 'Male', 10, 2016),
(15, 1, '6', '51', 'ftyjtyj', '', 'tfjftj', 'tfyjft', 'yjtfyj', 'ftyjftyjftj', 'tfyjj', '', '', 'Female', 7, 2018),
(16, 1, '4', '52', '345345', '', '345345', 'drhdrth', 'drthdrth', 'drth', 'drthdrth', '', '', 'Male', 10, 2016),
(17, 1, '6', '51', 'ftyjtyj', '', 'tfjftj', 'tfyjft', 'yjtfyj', 'ftyjftyjftj', 'tfyjj', '', '', 'Female', 7, 2018);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `data`
--
ALTER TABLE `data`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `students`
--
ALTER TABLE `students`
 ADD PRIMARY KEY (`id`), ADD KEY `data_id` (`data_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `data`
--
ALTER TABLE `data`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `students`
--
ALTER TABLE `students`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
