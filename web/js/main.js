var app = angular.module('formData', ['ui.mask']);

app.directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;
            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
            }                    
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   };
});


app.controller('FormController', ['$scope', function($scope){
	
}]);



jQuery(document).ready(function($) {
	studentsFess();
	$(document).on('click', '.add_student a', function(event) {
		event.preventDefault();
		var form = $('.student_fee:last-child').clone(),
			position = form.find('.student_number');
		form.find('.delete_student').show();
		position.text((parseInt(position.text()) + 1));
		$('.student_fees').append("<div class='student_fee'></div>");
		$('.student_fee:last-child').html(form.html()).find('input').each(function(index, el) {
			$(this).val('');
		}).closest('.student_fee').find('select').each(function(index, el) {
			$(this).val('');
		});;
		studentsFess();
	});
	$(document).on('click', '.delete_student a', function(event) {
		event.preventDefault();
		var els = [],
		    thisPosition = $(this).closest('.studentHead').find('.student_number').text();
		$(this).closest('.student_fee').remove(); 
		$('.student_fee').each(function(index, el) {
			var position = parseInt($(this).find('.student_number').text());
			if (position > thisPosition)
				$(this).find('.student_number').text((position - 1));
		});
		studentsFess();
	});
});
function studentsFess(){
	$('input[name="student_fee"]').val(55 * $('.student_fee').length);
}


var adminApp = angular.module('adminCrud', []);

adminApp.controller('CrudController', ['$scope', function($scope){
	
}]);